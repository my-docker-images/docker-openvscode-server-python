
FROM gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-openvscode-server:latest

## software needed

## python3 and python extension for VSCode already installed in base image

# For SIP TD (except Pistus)
RUN pip3 install jupyter numpy matplotlib

